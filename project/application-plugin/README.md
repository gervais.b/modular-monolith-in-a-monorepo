# modular-monolith-sbt-plugin

> An Sbt auto plugin to help in the development of a modular-monolith.

**Open it in you IDE from here, not from the root project**

## Conventions

A **module** is a couple of two folders with the same prefix: 
 - {name}-api
 - {name}-impl

## Features

### OpenAPI Code generation
For each module declared into the project build file, if an OpenAPI specification 
file is found with the  `.oas.yml` file extension from the module api folder, the 
classes are generated into that module implementation folder.

A dedicated router is generated with a constructor to inject the needed controllers. The controllers are generated as abstract classes. And all models 
are generated.

## TODO
