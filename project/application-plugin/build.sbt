ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.example"

lazy val root = (project in file("."))
  .enablePlugins(SbtPlugin)
  .settings(
    name := "application-plugin",
    libraryDependencies ++= Seq(
      "org.openapitools" % "openapi-generator" % "5.1.1",
      "io.swagger.parser.v3" % "swagger-parser" % "2.1.1",
      "com.samskivert" % "jmustache" % "1.15"
    ),
    pluginCrossBuild / sbtVersion := {
      scalaBinaryVersion.value match {
        case "2.12" => "1.2.8" // set minimum sbt version
      }
    }
  )
