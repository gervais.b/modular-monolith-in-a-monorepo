package module

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.parser.OpenAPIV3Parser
import sbt.ResolvedProject

import java.io.File

class Module(private val parent:File, val name:String) {
  def getOpenApiSpecification():Option[OpenAPI] = {
    val file =new File(api, s"src/main/oas/$name.oas.yaml")
    if ( file.isFile && file.exists() ) {
      Some(new OpenAPIV3Parser().read(file.getAbsolutePath))
    } else {
      None
    }
  }

  val api:File = new File(parent, s"$name-api")
  val impl:File = new File(parent, s"$name-impl")
}

object Module {
  def resolve(project: ResolvedProject):Either[Exception, Module] = {
    val folder = project.base
    if ( folder.getName.endsWith("-impl") ) {
      Right(new Module(
        folder.getParentFile, folder.getName.substring(0, folder.getName.indexOf("-impl"))
      ))
    } else {
      Left(new IllegalArgumentException(s"Module must be defined on implementation. Got ${project.base.getName}"))
    }
  }
}
