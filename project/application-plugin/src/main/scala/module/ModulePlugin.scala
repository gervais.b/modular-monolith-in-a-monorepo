package module

import openapi.{OpenApiGenerator, OpenApiGeneratorSettings}
import sbt.Keys.{organization, sLog, target, thisProject, sourceManaged}
import sbt.{AutoPlugin, Def, File, taskKey}

import java.util.function.Consumer
import scala.collection.mutable

/**
 * API implementation plugin provide default configuration, settings and
 * tasks to implement an API.
 *
 * This plugin rely on naming conventions to locate the matching API and generate
 * classes based on that API.
 */
object ModulePlugin extends AutoPlugin {

  implicit class RichCollection[A](private val it:java.util.Collection[A]) {
    def toSeq:Seq[A] = {
      val elements = mutable.ArrayBuffer[A]()
      it.forEach((element: A) => elements += element)
      elements
    }

  }

  object autoImport {
    val generateOpenApi = taskKey[Unit]("Generate models, controllers and routes from an OpenAPI specification")
  }

  import autoImport.*
  override def projectSettings: Seq[Def.Setting[_]] = Seq(
    generateOpenApi := {
      val logger = sLog.value
      val result = for {
        module <- Module.resolve(thisProject.value)
      } yield {
        module.getOpenApiSpecification().fold{
          logger.info(s"No specification found for ${module.name}")
        }{ spec =>
          logger.info(s"Generating OpenAPI classes in ${module.name} ")
          val settings = new OpenApiGeneratorSettings()
            .withTargetPackage(s"${organization.value}.${module.name.toLowerCase()}.http")
          new OpenApiGenerator(spec, settings)
          // FIXME change "demo" by "main"
            .generate(new File(sourceManaged.value, "demo"))
        }
      }
      result.fold(
        ex => throw ex,
        rs => ()
      )
    }
  )
}
