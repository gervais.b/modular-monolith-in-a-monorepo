package openapi;

import java.io.IOException;

class TemplateError extends OpenAPIGeneratorError {
     TemplateError(String action, Class template, Exception cause) {
        super(action+" "+template.getSimpleName()+" failed: "+cause.getMessage(), cause);
    }

    TemplateError(String action, String name, String cause) {
         super(action+" "+name+" failed: "+cause, null);
    }
}
