package openapi;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

abstract class Model {

    public String getGenerator() {
        return OpenApiGenerator.class.getSimpleName();
    }

    public String getTimestamp() {
        return DateTimeFormatter.ISO_LOCAL_DATE_TIME
                .format(LocalDateTime.now());
    }


}
