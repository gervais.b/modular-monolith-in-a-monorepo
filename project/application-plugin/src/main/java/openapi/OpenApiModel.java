package openapi;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OpenApiModel extends Model {
    private final OpenApiGeneratorSettings settings;
    private final OpenAPI spec;

    public OpenApiModel(OpenAPI spec, OpenApiGeneratorSettings settings) {
        this.settings = settings;
        this.spec = spec;
    }

    public Optional<RoutesModel> getRoutes() {
        return Optional.of(new RoutesModel(spec, settings));
    }

    public Stream<ControllerModel> getControllers() {
        Map<String, List<Operation>> operationsByTag = getOperations()
                .collect(Collectors.groupingBy(op -> firstOf(op.getTags()).orElse("Untagged")));
        return operationsByTag.entrySet().stream().map(e -> {
            String tag = e.getKey();
            return new ControllerModel(
                    Strings.toUpperCamelCase(tag+"Controller"),
                    e.getValue(), settings);
        });
    }

    private <E> Optional<E> firstOf(Iterable<E> elements) {
        Iterator<E> it = elements.iterator();
        if (it.hasNext() ) {
            return Optional.of(it.next());
        } else {
            return Optional.empty();
        }
    }

    private Stream<Operation> getOperations() {
        return spec.getPaths().values().stream()
                .flatMap(path -> {
                    List<Operation> operations = new ArrayList<>(7);
                    if ( path.getGet()!=null ) {
                        operations.add(path.getGet());
                    }
                    if ( path.getPost()!=null ) {
                        operations.add(path.getPost());
                    }
                    if ( path.getPut()!=null ) {
                        operations.add(path.getPut());
                    }
                    if (path.getDelete()!=null ) {
                        operations.add(path.getDelete());
                    }
                    if ( path.getPatch()!=null ) {
                        operations.add(path.getPatch());
                    }
                    if ( path.getOptions()!=null ) {
                        operations.add(path.getOptions());
                    }
                    if ( path.getHead()!=null ) {
                        operations.add(path.getHead());
                    }
                    return operations.stream();
                });
    }
}
