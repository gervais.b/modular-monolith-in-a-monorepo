package openapi;

import io.swagger.v3.oas.models.OpenAPI;

import java.io.File;
import java.util.Set;
import java.util.TreeSet;

/*
 * While this seems to be another rewrite of a wheel let me explain the
 * motivations behind this build support:
 *
 * We know and use the https://openapi-generator.tech/ project. However, that
 * one has some limitations regarding the generation of a multi-language project,
 * and we find the actual templating and extensions model too abstract for our
 * use cases.
 *
 * Writing yet another OpenAPI generator saved us a lot of time and debugging.
 */
public class OpenApiGenerator {

    private final OpenApiGeneratorSettings settings;
    private final OpenAPI spec;

    public OpenApiGenerator(OpenAPI spec, OpenApiGeneratorSettings settings) {
        this.settings = settings;
        this.spec = spec;
    }

    public void generate(File output) throws OpenAPIGeneratorError {
        OpenApiModel module = new OpenApiModel(spec, settings);
        File classes = settings.getClassesDirectory(output);
        module.getRoutes().ifPresent(m -> {
            new RoutesTemplate().merge(m, new File(classes, "Routes.scala"));
        });
        module.getControllers().forEach(m -> {
            new ControllerTemplate().merge(m, new File(classes, m.getName()+".scala"));
        });
    }
}
