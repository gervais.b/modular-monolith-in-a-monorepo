package openapi;

abstract class OpenAPIGeneratorError extends RuntimeException {
     OpenAPIGeneratorError(String message, Exception cause) {
        super(message, cause);
    }
}
