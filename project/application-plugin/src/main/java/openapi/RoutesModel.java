package openapi;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;

import java.util.HashSet;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

class RoutesModel extends Model {

    class Routee {
        private final Operation operation;
        private final String verb;

        public Routee(PathItem item) {
            if ( item.getGet() !=null ) {
                this.operation = item.getGet();
                this.verb = "GET";
            } else if ( item.getHead() !=null ) {
                this.operation = item.getHead();
                this.verb = "HEAD";
            } else if ( item.getOptions() !=null ) {
                this.operation = item.getOptions();
                this.verb = "OPTIONS";
            } else if ( item.getPatch() !=null ) {
                this.operation = item.getPatch();
                this.verb = "PATCH";
            } else if ( item.getPost() !=null ) {
                this.operation = item.getPost();
                this.verb = "POST";
            } else if ( item.getPut() !=null ) {
                this.operation = item.getPut();
                this.verb = "PUT";
            } else {
                throw new UnsupportedOperationException("Routee from "+item);
            }
        }
    }

    class Route  implements Comparable<Route> {
        final ParameterModel controller;
        private final String verb;
        private final String path;
        private final String action;

        Route(String verb, String path, Operation operation) {
            this.controller = ParameterModel.byType(
                    operation.getTags().stream().findFirst().map(tag -> tag+"Controller").orElse("UntaggedController"));
            this.action = controller.name+"."+operation.getOperationId();
            this.verb = verb;
            this.path = path;
        }

        ParameterModel getController() {
            return controller;
        }

        public String getVerb() {
            return verb;
        }

        public String getPath() {
            return path;
        }

        public String getAction() {
            return action;
        }

        @Override
        public int compareTo(Route other) {
            String left = getVerb()+getPath();
            String right = other.getVerb()+other.getPath();
            return left.compareTo(right);
        }
    }

    private final OpenApiGeneratorSettings settings;
    private final Set<ParameterModel> controllers;
    private final Set<Route> routes;

    public RoutesModel(OpenAPI spec, OpenApiGeneratorSettings settings) {
        this.settings = settings;
        this.routes = spec.getPaths().entrySet().stream().flatMap(entry -> {
            String path = entry.getKey();
            Set<Route> routes = parseRoutes(path, entry.getValue());
            return routes.stream();
        }).collect(toSet());
        this.controllers = routes.stream().map(route -> route.getController())
                .collect(toSet());
    }

    private Set<Route> parseRoutes(String path, PathItem item) {
        Set<Route> routes = new HashSet<>();
        if ( item.getDelete()!=null ) {
            routes.add(new Route("DELETE", path, item.getDelete()));
        }
        if ( item.getGet()!=null ) {
            routes.add(new Route("GET", path, item.getGet()));
        }
        if ( item.getHead()!=null ) {
            routes.add(new Route("HEAD", path, item.getHead()));
        }
        if ( item.getOptions()!=null ) {
            routes.add(new Route("OPTIONS", path, item.getOptions()));
        }
        if ( item.getPatch()!=null ) {
            routes.add(new Route("PATCH", path, item.getPatch()));
        }
        if ( item.getPost()!=null ) {
            routes.add(new Route("POST", path, item.getPost()));
        }
        if ( item.getPut()!=null ) {
            routes.add(new Route("PUT", path, item.getPut()));
        }
        return routes;
    }

    public String getPackage() {
        return settings.targetPackage;
    }

    public Set<ParameterModel> getControllers() {
        return controllers;
    }

    public Set<Route> getRoutes() {
        return routes;
    }
}
