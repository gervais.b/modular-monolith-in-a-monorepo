package openapi;

public abstract class Strings {

    private Strings() { /* */ }

    public static String toUpperCamelCase(String in) {
        StringBuilder out = new StringBuilder(in);
        for (int i = 0; i <out.length(); i++) {
            if ( i==0 ) {
                out.setCharAt(i, Character.toUpperCase(out.charAt(i)));
            } else if ( Character.isWhitespace(out.charAt(i-1)) ) {
                out.delete(i-1, i);
                out.setCharAt(i, Character.toUpperCase(out.charAt(i)));
            }
        }
        return out.toString();
    }

    public static String toLowerCamelCase(String in) {
        StringBuilder out = new StringBuilder(in);
        for (int i = 0; i <out.length(); i++) {
            if ( i==0 ) {
                out.setCharAt(i, Character.toLowerCase(out.charAt(i)));
            } else if ( Character.isWhitespace(out.charAt(i-1)) ) {
                out.delete(i-1, i);
                out.setCharAt(i, Character.toLowerCase(out.charAt(i)));
            }
        }
        return out.toString();
    }
}
