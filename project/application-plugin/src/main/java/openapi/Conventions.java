package openapi;

import java.io.File;

class Conventions {

    private final OpenApiGeneratorSettings settings;


    Conventions(OpenApiGeneratorSettings settings) {
        this.settings = settings;
    }

    File getClassesDirectory(File root) {
        var path = settings.targetPackage.replace('.', File.separatorChar);
        return new File(root, path);
    }

}
