package openapi;

import io.swagger.v3.oas.models.Operation;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ControllerModel extends Model {
    private final List<Operation> operations;
    private final String name;
    private OpenApiGeneratorSettings settings;

    public ControllerModel(String name, List<Operation> operations, OpenApiGeneratorSettings settings) {
        this.operations = operations;
        this.name = name;
        this.settings = settings;
    }

    public String getType() {
        return name;
    }

    public String getName() {
        return name;
    }

    public List<MethodModel> getMethods() {
        System.out.println(operations.size()+" methods");

        return operations.stream().map(MethodModel::new)
                .collect(Collectors.toList());
    }

    public String getPackage() {
        return settings.targetPackage;
    }

}
