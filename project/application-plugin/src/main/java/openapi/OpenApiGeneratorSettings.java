package openapi;

import java.io.File;

public class OpenApiGeneratorSettings {
    protected String targetPackage;
    protected Conventions conventions = new Conventions(this);

    public OpenApiGeneratorSettings() {
    }

    public OpenApiGeneratorSettings withTargetPackage(String targetPackage) {
        OpenApiGeneratorSettings copy = new OpenApiGeneratorSettings();
        copy.targetPackage = targetPackage;
        return copy;
    }

    public File getClassesDirectory(File root) {
        return conventions.getClassesDirectory(root);
    }

    @Override
    public String toString() {
        return "OpenApiGeneratorSettings{" +
                "targetPackage='" + targetPackage + '\'' +
                '}';
    }
}
