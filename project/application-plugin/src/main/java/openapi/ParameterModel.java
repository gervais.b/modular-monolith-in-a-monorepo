package openapi;

class ParameterModel extends Model {

    static ParameterModel byName(String name) {
        return new ParameterModel(name, Strings.toUpperCamelCase(name));
    }

    static ParameterModel byType(String type) {
        return new ParameterModel(Strings.toLowerCamelCase(type), type);
    }

    final String name;
    final String type;
    final boolean optional;

    public ParameterModel(String name, String type) {
        this(name, type, false);
    }
    public ParameterModel(String name, String type, boolean optional) {
        this.name = name;
        this.type = type;
        this.optional = optional;
    }


    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public boolean getOptional() {
        return optional;
    }

    @Override
    public String toString() {
        return "Parameter{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParameterModel parameter = (ParameterModel) o;

        if (!name.equals(parameter.name)) return false;
        return type.equals(parameter.type);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }
}
