package openapi;

import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.MustacheException;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Set;

abstract class Template {

    private final com.samskivert.mustache.Template template;
    private final String name = getClass().getSimpleName()+".mustache";
    public Template() {
        try (Reader reader = new InputStreamReader(getClass().getResourceAsStream(name))) {
            template = Mustache.compiler()
                    .withLoader(new ClasspathTemplateLoader())
                    .compile(reader);
        } catch (IOException e) {
            throw new TemplateError("Compilation of", getClass(), e);
        }
    }

    Set<File> merge(Object model, File file) {
        if ( !file.exists() ) {
            file.getParentFile().mkdirs();
        }
        try (Writer writer = new FileWriter(file, Charset.forName("UTF-8"), false)) {
            template.execute(model, writer);
            System.out.println("Template.merge -> Written, " + file.getAbsolutePath());
            writer.flush();
            return Collections.singleton(file);
        } catch (MustacheException e) {
            throw new TemplateError("Merge of "+model+" in ", getClass(), e);
        } catch (IOException e) {
            throw new TemplateError("Execution of", getClass(), e);
        }
    }

    private static class ClasspathTemplateLoader implements Mustache.TemplateLoader {

        @Override
        public Reader getTemplate(String name) throws Exception {
            InputStream stream = getClass().getResourceAsStream(name+".mustache");
            if ( stream==null ) {
                throw new TemplateError("Execution of", name, "Template not found");
            } else {
                return new InputStreamReader(stream);
            }

        }
    }
}
