package openapi;


import io.swagger.v3.oas.models.Operation;

import java.util.List;
import java.util.stream.Collectors;

public class MethodModel extends Model {
        private final Operation operation;
        public MethodModel(Operation operation) {
            this.operation = operation;
        }

        public String getName() {
            return operation.getOperationId();
        }

        public List<ParameterModel> getParameters() {
            if ( operation.getParameters()==null ) {
                return List.of();
            }

            return operation.getParameters().stream().map( p -> {
                boolean required = p.getRequired()==null?false:p.getRequired().booleanValue();
                return new ParameterModel(p.getName(), p.getSchema().getType(), required);
            }).collect(Collectors.toList());
        }

    }
