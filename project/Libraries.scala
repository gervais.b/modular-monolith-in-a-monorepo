object Libraries {
  import sbt.Keys._
  import sbt._

  val Liquibase = Seq(
    "org.liquibase" % "liquibase-core" % "4.14.0",
    "org.liquibase.ext" % "liquibase-cassandra" % "4.14.0"
  )

  val Playframework = Seq(
    "com.typesafe" % "config" % "1.4.2",
    "com.typesafe.play" %% "play" % "2.8.16"
  )

  val AkkActorTyped = Seq(
    "com.typesafe.akka" %% "akka-actor-typed" % "2.6.19"
  )

  val AkkaPersistence = Seq(
    "com.lightbend.akka" %% "akka-projection-eventsourced" % "1.2.4",
    "com.lightbend.akka" %% "akka-projection-jdbc" % "1.2.4",
    "com.lightbend.akka" %% "akka-projection-cassandra" % "1.2.4",
    "com.typesafe.akka" %% "akka-persistence-cassandra" % "1.0.5",
    "com.typesafe.akka" %% "akka-cluster-sharding-typed" % "2.6.19",
    "com.typesafe.akka" %% "akka-persistence-typed" % "2.6.19",
    "com.typesafe.akka" %% "akka-cluster-typed" % "2.6.19",
    "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8"
  ) ++ AkkActorTyped

  val H2Database = Seq(
    "com.h2database" % "h2" % "2.1.214"
  )

  val Scalatest = Seq(
    "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test
  )

}
