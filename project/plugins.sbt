addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.16")

/* This is a trick to add the content of the `module-plugin` as a project.
 * Since this project is an Sbt auto plugin it can be applied on projects
 * in this mono repository.
 */
lazy val `build` = (project in file("."))
  .dependsOn(`module-plugin`)
lazy val `module-plugin` = RootProject(file("./application-plugin"))
