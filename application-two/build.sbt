ThisBuild / scalaVersion := "2.13.8"

ThisBuild / version := "1.0-SNAPSHOT"

ThisBuild / organization := "com.example"

import Libraries._

lazy val `project-two` = (project in file("."))
  .aggregate(
    `webapi`
  )

lazy val `webapi` = (project in file("webapi"))
  .enablePlugins(PlayScala)
  // Use `src/main` layout instead of `app`
  .disablePlugins(PlayLayoutPlugin)
  .settings(
    libraryDependencies ++= Scalatest
  )
