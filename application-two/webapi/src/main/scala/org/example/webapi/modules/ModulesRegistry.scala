package org.example.webapi.modules

import java.util.jar
import java.util.jar.Attributes

object ModulesRegistry {
  import scala.jdk.CollectionConverters._

  private val thisVendor = {
    val fqn = getClass.getName
    val index = fqn.indexOf('.', 4)
    fqn.substring(0, index)
  }
  private def sameVendor = (m: jar.Manifest) => {
    val impl = m.getMainAttributes.getValue("Implementation-Vendor-Id")
    impl != null && impl.startsWith(thisVendor)
  }

  private val context = Thread.currentThread().getContextClassLoader
  private val modules: Map[String, Module] = {
    val resources = context.getResources("META-INF/MANIFEST.MF").asScala
    val manifests = resources.map(r => new jar.Manifest(r.openStream()))
      .filter(sameVendor)

    manifests
      .map(m => new Module(m))
      .map { m => (m.name -> m) }
      .toMap
  }

  def all(): Set[Module] = modules.values.toSet

  def isEnabled(name:String) = modules.contains(name)
}
