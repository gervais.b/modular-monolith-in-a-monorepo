package controllers

import org.example.webapi.modules.ModulesRegistry

import javax.inject._
import play.api._
import play.api.mvc._

/** This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */
@Singleton
class HomeController @Inject() (val controllerComponents: ControllerComponents)
    extends BaseController {

  /** Create an Action to render an HTML page.
    *
    * The configuration in the `routes` file means that this method
    * will be called when the application receives a `GET` request with
    * a path of `/`.
    */
  def index() = Action { implicit request: Request[AnyContent] =>
    val modules = ModulesRegistry.all()

    Ok(s"""
        |Welcome home.
        |
        |There are ${modules.size} module enabled: 
        |  ${modules.map(m => s"${m.name} v${m.version}").mkString("\t", "\n\t", "\n")}
        |""".stripMargin)
  }
}
