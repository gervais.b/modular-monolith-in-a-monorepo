package org.example.runtime;

import akka.actor.typed.ActorRef;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.Props;
import org.example.EventBus;

import java.util.function.Consumer;


public class AkkaEventBus implements EventBus {

    private final ActorRef<AkkaEventBusActor.Command> actor;

    public AkkaEventBus(ActorSystem<?> system) {
        actor = system.systemActorOf(
                AkkaEventBusActor.create(),
                AkkaEventBusActor.class.getName(),
                Props.empty());
    }

    @Override
    public <T> void on(Class<T> selector, Consumer<T> handler) {
        Consumer<Object> consumer = (Consumer<Object>) handler;
        actor.tell(new AkkaEventBusActor.Register(consumer, selector));
    }

    @Override
    public void publish(Object event) {
        actor.tell(new AkkaEventBusActor.Publish(event));
    }
}
