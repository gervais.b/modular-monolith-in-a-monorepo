package org.example.runtime;

import akka.Done;
import akka.actor.typed.ActorSystem;
import akka.cluster.typed.ClusterSingleton;
import akka.cluster.typed.SingletonActor;
import akka.persistence.cassandra.query.javadsl.CassandraReadJournal;
import akka.persistence.query.Offset;
import akka.projection.Projection;
import akka.projection.ProjectionBehavior;
import akka.projection.ProjectionId;
import akka.projection.cassandra.javadsl.CassandraProjection;
import akka.projection.eventsourced.EventEnvelope;
import akka.projection.eventsourced.javadsl.EventSourcedProvider;
import akka.projection.javadsl.Handler;
import akka.projection.javadsl.SourceProvider;
import org.example.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;

/**
 * A simplified Akka projection that will be used to publish events on an
 * {@link EventBus}.
 */
public abstract class AkkaEventPublisher<E> {
    private final Logger log = LoggerFactory.getLogger(getClass());

    private final ActorSystem<?> system;
    private final EventBus bus;

    public AkkaEventPublisher(ActorSystem<?> system, EventBus bus) {
        this.system = system;
        this.bus = bus;

        var projection = projection();
        ClusterSingleton.get(system).init(
                SingletonActor.of(
                        ProjectionBehavior.create(projection),
                        projection.projectionId().id()
                ).withStopMessage(ProjectionBehavior.stopMessage())
        );
        log.info("Projection on {} started", tag());
    }

    protected abstract String tag();

    protected void publish(EventEnvelope<E> envelope) {
        log.info("Publishing {} from {} (Seq.Nr.: {})",
                envelope.event().getClass().getName(), envelope.persistenceId(),
                envelope.sequenceNr());
        bus.publish(envelope.event());
    }

    private SourceProvider<Offset, EventEnvelope<E>> source() {
        return EventSourcedProvider.eventsByTag(
                system, CassandraReadJournal.Identifier(), tag());
    }

    private Projection<EventEnvelope<E>> projection() {
        var handler = new Handler<EventEnvelope<E>>() {
            @Override
            public CompletionStage<Done> process(EventEnvelope<E> envelope) throws Exception, Exception {
                return CompletableFuture.supplyAsync(() -> {
                    publish(envelope);
                    return Done.done();
                });
            }
        };
        return CassandraProjection.atLeastOnce(
                ProjectionId.of(getClass().getName(), tag()),
                source(),
                () -> handler
        ).withSaveOffset(100, Duration.ofMillis(500));
    }
}
