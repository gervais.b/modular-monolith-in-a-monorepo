package org.example.runtime;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.Behaviors;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

class AkkaEventBusActor {

    static final Behavior<Command> create() {
        return behavior(new AkkaEventBusActor(new HashMap<>(1)));
    }

    private static Behavior<Command> behavior(AkkaEventBusActor bus) {
        return Behaviors.receive(Command.class)
                .onMessage(Register.class, (cmd) -> {
                    return behavior(bus.onRegister(cmd));
                })
                .onMessage(Publish.class, (cmd) -> {
                    bus.onPublish(cmd);
                    return Behaviors.same();
                })
                .build();
    }

    interface Command {
    }

    static class Register implements Command {
        private final Consumer<Object> consumer;
        private final Class<?> selector;

        Register(Consumer<Object> consumer, Class<?> selector) {
            this.consumer = consumer;
            this.selector = selector;
        }
    }

    static class Publish implements Command {
        private final Object event;

        Publish(Object event) {
            this.event = event;
        }
    }


    private final HashMap<Class<?>, Set<Consumer<Object>>> registrations = new HashMap<>();

    private AkkaEventBusActor(Map<Class<?>, Set<Consumer<Object>>> registrations) {
        this.registrations.putAll(registrations);
    }

    private AkkaEventBusActor onRegister(Register cmd) {
        registrations.merge(cmd.selector, Set.of(cmd.consumer), (l, r) -> {
            l.addAll(r);
            return l;
        });
        return new AkkaEventBusActor(registrations);
    }

    private void onPublish(Publish cmd) {
        var selector = cmd.event.getClass();
        registrations.get(selector).forEach(c -> c.accept(cmd.event));
    }
}
