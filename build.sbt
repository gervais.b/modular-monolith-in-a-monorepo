ThisBuild / scalaVersion := "2.13.8"

ThisBuild / version := "1.0-SNAPSHOT"

ThisBuild / organization := "com.example"

lazy val `example-system` = (project in file("."))

lazy val `project-one` = (project in file("project-one"))

lazy val `project-two` = (project in file("project-two"))
