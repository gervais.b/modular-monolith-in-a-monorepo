package org.example.http

import org.example.security.AuthenticationService
import org.example.security.AuthenticationService.{BearerToken, EmailPasswordToken, Token}
import org.slf4j.LoggerFactory
import play.api.mvc._

import java.util.Base64
import javax.security.auth.Subject
import scala.concurrent.{ExecutionContext, Future}

/**
  * Provide the `Authenticated` action builder to use in our controllers.
  *
  * This trait requires the ControllerComponents and an AuthenticationService
  * instance.
  */
trait Authentication {
  import Authentication._

  protected def controllerComponents: ControllerComponents
  protected def authenticationService: AuthenticationService

  // noinspection ScalaStyle  This will look better into the controller
  def Authenticated: AuthenticatedActionBuilder =
    new AuthenticatedActionBuilder(
      controllerComponents.parsers.anyContent,
      controllerComponents.executionContext,
      authenticationService
    )
}

object Authentication {
  private val log = LoggerFactory.getLogger(classOf[Authentication])
  private val BearerAuthenticationSchema = "Bearer\\s+(.+)".r
  private val BasicAuthenticationSchema = "Basic\\s+(.+)".r

  class AuthenticatedRequest[A](
      val subject: Subject,
      request: Request[A]
  ) extends WrappedRequest[A](request)

  class AuthenticatedActionBuilder(
      val parser: BodyParser[play.api.mvc.AnyContent],
      implicit val executionContext: ExecutionContext,
      val provider: AuthenticationService
  ) extends ActionBuilder[AuthenticatedRequest, AnyContent] {

    private def getAuthorizationToken(
        request: RequestHeader
    ): Option[Token] = {
      val authorization: Option[String] = request.headers.get("Authorization")
      val token: Option[AuthenticationService.Token] = authorization.map {
        case BearerAuthenticationSchema(value) =>
          new BearerToken(value)
        case BasicAuthenticationSchema(token) =>
          val (username, password) =
            Base64.getDecoder.decode(token).span(b => b == ':')
          new EmailPasswordToken(
            new String(username, "UTF-8"),
            password.map(_.toChar)
          )
      }
      token
    }

    private def getAuthenticatedSubject(token: Token): Option[Subject] = {
      val result = provider.authenticate(token)
      if (result.isPresent)
        Some(result.get())
      else
        None
    }

    private def authenticate[A](
        request: Request[A]
    ): Option[Subject] = {
      for {
        token <- getAuthorizationToken(request)
        subject <- getAuthenticatedSubject(token)
      } yield {
        subject
      }
    }

    type Body[A] = AuthenticatedRequest[A] => Future[Result]

    def invokeBlock[A](request: Request[A], block: Body[A]): Future[Result] = {
      authenticate(request) match {
        case Some(subject) =>
          block(new AuthenticatedRequest(subject, request))
        case None =>
          Future.successful(
            Results.Unauthorized
          )
      }
    }

  }

}
