
CREATE TABLE IF NOT EXISTS akka_projection.projection_management (
    projection_name
    text,
    partition
    int,
    projection_key
    text,
    paused
    boolean,
    last_updated
    timestamp,
    PRIMARY
    KEY (
        (projection_name, partition),
        projection_key
    )
);
