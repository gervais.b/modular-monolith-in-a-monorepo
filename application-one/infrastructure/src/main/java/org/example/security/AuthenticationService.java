package org.example.security;

import javax.security.auth.Subject;
import java.util.Optional;

public interface AuthenticationService {

    interface Token {}

    class EmailPasswordToken implements Token {
        protected final String email;
        protected final char[] password;

        public EmailPasswordToken(String email, char[] password) {
            this.email = email;
            this.password = password;
        }
    }

    class BearerToken implements Token {
        protected final String value;

        public BearerToken(String value) {
            this.value = value;
        }
    }

    Optional<Subject> authenticate(Token token);
}
