package org.example.security;

import javax.security.auth.Subject;

public interface AuthorizationService {

    boolean isAuthorized(Subject subject, Operation operation);
}
