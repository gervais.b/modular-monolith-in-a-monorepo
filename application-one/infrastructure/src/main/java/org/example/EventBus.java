package org.example;

import java.util.function.Consumer;


public interface EventBus {

    <T> void on(Class<T> selector, Consumer<T> handler);

    void publish(Object event);

}
