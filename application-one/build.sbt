ThisBuild / scalaVersion := "2.13.8"

ThisBuild / version := "1.0-SNAPSHOT"

ThisBuild / organization := "com.example"

import Libraries._

lazy val `project-one` = (project in file("."))
  .aggregate(
    `infrastructure`,
    `preferences-impl`,
    `registration-api`,
    `registration-impl`,
    `support`,
    `webapi`,
    `project-plugins`
  )

lazy val `project-plugins` = (project in file("project-plugins"))
  .settings(
    libraryDependencies ++= Seq(
      "io.swagger.parser.v3" % "swagger-parser" % "2.1.1",
      "com.samskivert" % "jmustache" % "1.15"
    )
  )

lazy val `infrastructure` = (project in file("infrastructure"))
  .settings(
    libraryDependencies ++= Liquibase ++ Playframework
  )

lazy val `support` = (project in file("support"))
  .settings(
    libraryDependencies ++= AkkaPersistence
  )
  .dependsOn(`infrastructure`)

lazy val `registration-api` = (project in file("registration-api"))
  .settings(
    libraryDependencies ++= AkkActorTyped
  )

lazy val `registration-impl` = (project in file("registration-impl"))
  .enablePlugins(ModulePlugin)
  .settings(
    libraryDependencies ++= Playframework ++ AkkaPersistence ++ H2Database
  )
  .dependsOn(`registration-api`, `support`)

lazy val `preferences-api` = (project in file("preferences-api"))

lazy val `preferences-impl` = (project in file("preferences-impl"))
  .enablePlugins(ModulePlugin)
  .dependsOn(`preferences-api`, `registration-api`, `infrastructure`)

lazy val `webapi` = (project in file("webapi"))
  .enablePlugins(PlayScala)
  // Use `src/main` layout instead of `app`
  .disablePlugins(PlayLayoutPlugin)
  .settings(
    libraryDependencies ++= Scalatest
  )
  .dependsOn(
    `infrastructure`,
    `support`,
    `registration-impl`,
    `preferences-impl`
  )
