package org.example.webapi

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.adapter.ClassicActorSystemOps
import com.typesafe.config.Config
import org.example.preferences.PreferencesModule
import org.example.registration.RegistrationModule
import org.example.runtime.AkkaEventBus
import play.api.ApplicationLoader.Context
import play.api.BuiltInComponentsFromContext
import play.api.libs.concurrent.AkkaTypedComponents
import play.api.routing.Router
import play.filters.HttpFiltersComponents

class WebApiComponents(context: Context)
    extends BuiltInComponentsFromContext(context)
    with AkkaTypedComponents
    with HttpFiltersComponents {


  private val config: Config = application.configuration.underlying
  private val system: ActorSystem[Nothing] = actorSystem.toTyped

  private val eventBus = new AkkaEventBus(system)
  private val registration = new RegistrationModule(eventBus, system)
  private val preferences = new PreferencesModule(eventBus)

  override def router: Router = {
    new WebApiRouter(controllerComponents)
      .orElse(registration.router(controllerComponents))
      .orElse(preferences.router(controllerComponents))
  }
}
