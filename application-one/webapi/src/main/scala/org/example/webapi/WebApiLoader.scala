package org.example.webapi

import org.example.runtime.CassandraDatabase
import play.api.ApplicationLoader.Context
import play.api.{Application, ApplicationLoader, LoggerConfigurator}

import scala.util.Try

class WebApiLoader extends ApplicationLoader {
  private def configureLogging(context: Context): Try[Unit] = Try {
    LoggerConfigurator(context.environment.classLoader).foreach {
      _.configure(context.environment, context.initialConfiguration, Map.empty)
    }
  }

  private def migrateDatabase(context: Context): Try[Unit] = Try {
    new CassandraDatabase(context.initialConfiguration.underlying).update()
  }

  override def load(context: Context): Application = {
    (for {
      _ <- configureLogging(context)
      _ <- migrateDatabase(context)
    } yield {
      new WebApiComponents(context).application
    }).get
  }
}
