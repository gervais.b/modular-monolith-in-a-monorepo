package org.example.webapi

import controllers.HomeController
import play.api.mvc._
import play.api.routing.Router.Routes
import play.api.routing.SimpleRouter
import play.api.routing.sird._

class WebApiRouter(cc: ControllerComponents) extends SimpleRouter {
  private val ctrl = new HomeController(cc)
  override def routes: Routes = { case GET(p"/") =>
    ctrl.index()
  }
}
