package org.example.webapi.modules

class Module(private val manifest: java.util.jar.Manifest) {
  private val entries = manifest.getMainAttributes

  val name: String = entries.getValue("Name")
  val title: String = entries.getValue("Implementation-Tile")
  val version: String = entries.getValue("Implementation-Version")

  override def toString: String = {
    s"${getClass.getSimpleName}($name, $version)"
  }
}
