package org.example.runtime;

class CassandraDatabaseUpdateFailed extends RuntimeException {
    public CassandraDatabaseUpdateFailed(String message, Exception cause) {
        super(message, cause);
    }
}
