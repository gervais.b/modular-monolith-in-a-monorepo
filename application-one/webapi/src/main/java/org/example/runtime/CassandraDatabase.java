package org.example.runtime;

import com.typesafe.config.Config;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.Scope;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class CassandraDatabase {

    /* The driver class name required by Liquibase to connect with Cassandra */
    private static final String LIQUIBASE_CASSANDRA_JDBC_DRIVER = "com.simba.cassandra.jdbc42.Driver";
    private static final String LIQUIBASE_CASSANDRA_DATABASE_CLASS = "liquibase.ext.cassandra.database.CassandraDatabase";
    private static final String LIQUIBASE_CHANGELOG_PATH = "cassandra_changelog.xml";
    private static final String LIQUIBASE_KEYSPACE = "liquibase";

    private static final Logger LOG = LoggerFactory.getLogger(CassandraDatabase.class);

    private final Config config;

    private final String hostAndPort;
    private final String username;


    public CassandraDatabase(Config config) {
        this.config = config;

        this.hostAndPort = "localhost:9042";
        this.username = "cassandra";
    }

    private Map<String, Object> liquibaseConfig() {
        HashMap<String, Object> map = new HashMap<>();
        if ( config.hasPath("cassandra.liquibase") ) {
            var node = config.getConfig("cassandra.liquibase");
            node.entrySet().forEach(e -> {
                map.put(e.getKey(), node.getAnyRef(e.getKey()));
            });
        }
        return map;
    }

    /**
     * Apply the changelog to get the desired state on this Cassandra database.
     * @throws Exception
     */
    public void update() throws Exception {
        Scope.child(liquibaseConfig(), ()-> {
            LOG.info("Migrating Cassandra on {}", hostAndPort);
            initiateLiquibaseKeyspace();
            updateDatabase();
        });
    }

    private void initiateLiquibaseKeyspace() throws CassandraDatabaseUpdateFailed {
        /* We rely on Liquibase to do the update. However, it needs a default
         * keyspace that it cannot create by itself (At least I did not find
         * how). So the first update step will be to create this keyspace so
         * that Liquibase can do his work. */
        try {
            Class.forName(LIQUIBASE_CASSANDRA_JDBC_DRIVER);
            try (Connection conn = DriverManager.getConnection("jdbc:cassandra://"+hostAndPort)){
                var statement = conn.createStatement();
                statement.execute("CREATE KEYSPACE IF NOT EXISTS liquibase " +
                        "WITH REPLICATION = { 'class' : 'SimpleStrategy','replication_factor':1 };");
            }
        } catch (Exception e) {
            throw new CassandraDatabaseUpdateFailed("Unable to create liquibase keyspace", e);
        }
    }

    private Database createDatabase() {
        try {
            String url = String.format("jdbc:cassandra://%1$s/%2s;DefaultKeySpace=%2$s",
                hostAndPort, LIQUIBASE_KEYSPACE);
            Properties properties = new Properties();
            properties.putAll(liquibaseConfig());
            return DatabaseFactory.getInstance().openDatabase(
                    url,
                    username,
                    LIQUIBASE_CASSANDRA_JDBC_DRIVER,
                    LIQUIBASE_CASSANDRA_DATABASE_CLASS,
                    properties,
                    new ClassLoaderResourceAccessor()
            );
        } catch (DatabaseException e) {
            throw new CassandraDatabaseUpdateFailed("Failed to create database", e);
        }
    }

    private void updateDatabase() throws CassandraDatabaseUpdateFailed {
        try (Liquibase liquibase = new Liquibase(LIQUIBASE_CHANGELOG_PATH, new ClassLoaderResourceAccessor(), createDatabase())){
            var contexts = new Contexts();
            liquibase.update(contexts, new LabelExpression());

            var status = new StringWriter();
            liquibase.reportStatus(true, contexts, status);
            LOG.info("Cassandra update finished: "+status);
        } catch (Exception e) {
            throw new CassandraDatabaseUpdateFailed("Failed to execute liquibase update", e);
        }
    }

}
