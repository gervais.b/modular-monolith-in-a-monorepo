package org.example.preferences

import org.example.EventBus
import org.example.preferences.http.{PreferencesControllerImpl, Routes}
import org.example.registration.RegistrationApi.RegistrationCompleted
import org.slf4j.LoggerFactory
import play.api.mvc.ControllerComponents
import play.api.routing.Router

class PreferencesModule(events: EventBus) {

  private val log = LoggerFactory.getLogger(classOf[PreferencesModule])

  events.on[RegistrationCompleted](
    classOf[RegistrationCompleted],
    e => {

      log.info(s"Initializing preferences for ${e.user.email}")
    }
  )

  def router(cc: ControllerComponents): Router = {
    new Routes(new PreferencesControllerImpl(cc))
  }

}
