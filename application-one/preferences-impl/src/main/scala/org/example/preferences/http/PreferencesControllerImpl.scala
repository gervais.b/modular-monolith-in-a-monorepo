package org.example.preferences.http

import play.api.mvc.ControllerComponents

import scala.concurrent.Future

class PreferencesControllerImpl(cc: ControllerComponents)
    extends PreferencesController(cc) {
  override protected def getUserPreferences(ref: String, locale: Option[String]) = ???

  override protected def setUserPreferences(ref: String, request: PreferenceRequest) = ???
}
