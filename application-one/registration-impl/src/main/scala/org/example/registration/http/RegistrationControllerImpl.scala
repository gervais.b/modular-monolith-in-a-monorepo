package org.example.registration.http

import org.example.registration.RegistrationApi.AlreadyRegisteredError
import org.example.registration.RegistrationService
import play.api.Logger
import play.api.mvc.{AbstractController, ControllerComponents}

import java.util.concurrent.CompletionException
import scala.concurrent.ExecutionContext
import scala.jdk.FutureConverters.CompletionStageOps

class RegistrationControllerImpl(
    cc: ControllerComponents,
    svc: RegistrationService
) extends RegistrationController(cc) {

  override protected def startRegistration(
      startRegistration: RegistrationController,
      request: RegistrationRequest
  ) = {
    log.info(s"Got registration request for ${request.email}")
    val response = for {
      result <- svc.initiateRegistration(request.email).asScala
    } yield {
      Accepted(s"Registered, ${result.user.email}")
    }
    response.recover {
      case e: CompletionException =>
        InternalServerError(e + ":" + e.getMessage)
      case e: AlreadyRegisteredError =>
        Conflict(e.getMessage)
    }
  }
}
