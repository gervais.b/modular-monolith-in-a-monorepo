package org.example.registration;

import akka.actor.typed.ActorSystem;
import org.example.EventBus;
import org.example.akka.AkkaEventPublisher;

class RegistrationEventsPublisher extends AkkaEventPublisher<RegistrationApi.Event> {


    RegistrationEventsPublisher(ActorSystem<?> system, EventBus bus) {
        super(system, bus);
    }

    @Override
    protected String tag() {
        return RegistrationApi.RegistrationCompleted.class.getName();
    }
}
