package org.example.registration;

import akka.actor.typed.ActorSystem;
import akka.cluster.sharding.typed.javadsl.ClusterSharding;
import akka.cluster.sharding.typed.javadsl.Entity;
import akka.persistence.typed.PersistenceId;
import org.example.EventBus;
import org.example.registration.http.RegistrationControllerImpl;
import org.example.registration.http.Routes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.api.mvc.ControllerComponents;
import play.api.routing.Router;

public class RegistrationModule {
    private final Logger log = LoggerFactory.getLogger(RegistrationModule.class);

    private final EventBus eventBus;
    private final ClusterSharding sharding;

    public RegistrationModule(EventBus eventBus, ActorSystem<?> system) {
        this.eventBus = eventBus;
        this.sharding = ClusterSharding.get(system);
        sharding.init(Entity.of(RegistrationAr.ENTITY_TYPE_KEY, ctxt -> {
            var persistenceId = PersistenceId.of(
                    ctxt.getEntityTypeKey().name(),
                    ctxt.getEntityId());
            return RegistrationAr.create(persistenceId);
        }));

        new RegistrationEventsPublisher(system, eventBus);
    }

    public RegistrationService service() {
        return new RegistrationServiceImpl(email -> {
            log.info("Creating entity ref for {}", email);
            return sharding.entityRefFor(RegistrationAr.ENTITY_TYPE_KEY, email);
        });
    }

    public Router router(ControllerComponents cc) {
        return new Routes(new RegistrationControllerImpl(cc, service()));
    }
}
