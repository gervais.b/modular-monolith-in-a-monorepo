package org.example.registration;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.pattern.StatusReply;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.CommandHandlerWithReply;
import akka.persistence.typed.javadsl.EventHandler;
import akka.persistence.typed.javadsl.EventSourcedBehaviorWithEnforcedReplies;
import akka.persistence.typed.javadsl.ReplyEffect;

import java.time.Instant;
import java.util.*;

class RegistrationAr
        extends EventSourcedBehaviorWithEnforcedReplies<RegistrationApi.Command, RegistrationApi.Event, RegistrationAr.State>
        implements RegistrationApi {

    abstract class State {}

    private class Unregistered extends State {}

    private class Registered extends State {
        private final User user;

        Registered(User user) {
            this.user = user;
        }
    }

    private class Deactivated extends State {
        private final User user;

        Deactivated(User user) {
            this.user = user;
        }
    }


    static final List<String> TAGS = List.of("registration");
    static final EntityTypeKey<Command> ENTITY_TYPE_KEY =
            EntityTypeKey.create(Command.class, "registration::registration-ar");
    public static Behavior<Command> create(PersistenceId persistenceId) {
        return Behaviors.setup(context -> new RegistrationAr(context, persistenceId));
    }

    private final ActorContext<Command> context;

    private RegistrationAr(ActorContext<Command> context, PersistenceId persistenceId) {
        super(persistenceId);
        this.context = context;
        context.getLog().info("Starting RegistrationAR {}", persistenceId);
    }

    @Override
    public State emptyState() {
        return new Unregistered();
    }

    @Override
    public Set<String> tagsFor(Event event) {
        return Set.of(event.getClass().getName());
    }

    @Override
    public CommandHandlerWithReply<Command, Event, State> commandHandler() {
        var builder = newCommandHandlerWithReplyBuilder();
        builder.forStateType(Unregistered.class)
            .onCommand(Register.class, this::onRegister)
            .onCommand(Deactivate.class, this::onDeactivate);

        builder.forStateType(Registered.class)
            .onCommand(Get.class, this::onGet)
            .onCommand(Deactivate.class, this::onDeactivate);

        builder.forAnyState()
            .onCommand(Register.class, (cmd) -> Effect().reply(cmd.replyTo, StatusReply.error(new AlreadyRegisteredError(cmd.email))))
            .onCommand(Get.class, (cmd) -> Effect().reply(cmd.replyTo, StatusReply.error(new UnregisteredError())));

        return builder.build();
    }

    private ReplyEffect<Event, State> onRegister(Unregistered state, Register cmd) {
        context.getLog().info("Registering {}", cmd.email);
        var event = new RegistrationCompleted(Instant.now(), new User(UserRef.next(), cmd.email));
        return Effect()
                .persist(event)
                .thenReply(cmd.replyTo, (s) -> {
                    context.getLog().info("User registered: {}", event);
                    return StatusReply.success(event);
                });
    }

    private ReplyEffect<Event, State> onGet(Registered state, Get cmd) {
        return Effect().reply(cmd.replyTo, StatusReply.success(state.user));
    }

    private ReplyEffect<Event, State> onDeactivate(Unregistered state, Deactivate cmd) {
        return Effect().reply(cmd.replyTo, StatusReply.ack());
    }

    private ReplyEffect<Event, State> onDeactivate(Registered state, Deactivate cmd) {
        var event = new RegistrationDeactivated(Instant.now(), state.user);
        return Effect().persist(event).thenReply(cmd.replyTo, s -> StatusReply.ack());
    }

    @Override
    public EventHandler<State, Event> eventHandler() {
        var builder = newEventHandlerBuilder();

        builder.forStateType(Unregistered.class)
            .onEvent(RegistrationCompleted.class, (evt) -> new Registered(evt.user));

        builder.forAnyState()
                .onAnyEvent((state, evt) -> {
                    // FIXME, how to handle unexpected event ?
                    return state;
                });

        return builder.build();
    }

}
