package org.example.registration;

import akka.cluster.sharding.typed.javadsl.EntityRef;
import akka.pattern.StatusReply;
import org.example.registration.RegistrationApi.Command;
import org.example.registration.RegistrationApi.RegistrationCompleted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

class RegistrationServiceImpl implements RegistrationService {
    private final Logger logger = LoggerFactory.getLogger(RegistrationServiceImpl.class);

    private final Function<String, EntityRef<Command>> arFactory;
    // FIXME: Get timeout from config. (See https://developer.lightbend.com/docs/akka-platform-guide/microservices-tutorial/entity.html#_send_commands_to_the_entities)
    private final Duration timeout = Duration.ofSeconds(5);

    public RegistrationServiceImpl(Function<String, EntityRef<Command>> arFactory) {
        this.arFactory = arFactory;
    }

    public CompletionStage<RegistrationCompleted> startRegistration(String email) {
        EntityRef<Command> aggregate = arFactory.apply(email);
        logger.info("Registering user with {} on {}", email, aggregate);
        CompletionStage<StatusReply<RegistrationCompleted>> result =
                aggregate.ask(replyTo -> {
                    logger.info("Sending register command with replyTo {}", replyTo);
                    return new RegistrationApi.Register(email, replyTo);
                }, timeout);
        return result.thenApply(status -> status.getValue());
    }
}
