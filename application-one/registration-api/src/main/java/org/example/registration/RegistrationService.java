package org.example.registration;

import java.util.concurrent.CompletionStage;

public interface RegistrationService {

    /**
     * Initiate the registration process. Registering an user require the
     * validation of his email address by giving a code previously sent to that
     * email address.
     *
     * @param email
     * @return An empty completion stage that complete when the action
     */
    CompletionStage<Void> initiateRegistration(EmailAddress email);



}
