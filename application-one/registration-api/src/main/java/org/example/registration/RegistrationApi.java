package org.example.registration;

import akka.Done;
import akka.actor.typed.ActorRef;
import akka.pattern.StatusReply;

import java.io.Serializable;
import java.time.Instant;

public interface RegistrationApi {

    interface Command extends Serializable {}

    class Register implements Command {
        final String email;
        final ActorRef<StatusReply<RegistrationCompleted>> replyTo;

        public Register(String email, ActorRef<StatusReply<RegistrationCompleted>> replyTo) {
            this.email = email;
            this.replyTo = replyTo;
        }
    }

    class Get implements Command {
        final ActorRef<StatusReply<User>> replyTo;

        public Get(ActorRef<StatusReply<User>> replyTo) {
            this.replyTo = replyTo;
        }
    }

    class Deactivate implements Command {
        final ActorRef<StatusReply<Done>> replyTo;

        Deactivate(ActorRef<StatusReply<Done>> replyTo) {
            this.replyTo = replyTo;
        }
    }

    interface Event extends Serializable {}

    class RegistrationCompleted implements Event {
        public final Instant instant;
        public final User user;

        RegistrationCompleted(Instant instant, User beneficiary) {
            this.instant = instant;
            this.user = beneficiary;
        }
    }

    class RegistrationDeactivated implements Event {
        public final Instant instant;
        public final User user;

        RegistrationDeactivated(Instant instant, User user) {
            this.instant = instant;
            this.user = user;
        }
    }

    abstract class Error extends Throwable implements Serializable {}
    class UnregisteredError extends Error {}

    class AlreadyRegisteredError extends Error {
        public final String email;

        public AlreadyRegisteredError(String email) {
            this.email = email;
        }

        @Override
        public String getMessage() {
            return "User already registered: "+ email;
        }
    }
}
