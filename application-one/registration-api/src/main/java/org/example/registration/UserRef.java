package org.example.registration;

import java.io.Serializable;
import java.util.UUID;

public class UserRef implements Serializable {
    public static UserRef next() {
        return new UserRef(UUID.randomUUID());
    }

    private final UUID value;
    private UserRef(UUID value) {
        this.value = value;
    }
}
