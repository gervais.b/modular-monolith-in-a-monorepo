package org.example.registration;

import java.io.Serializable;

public class User implements Serializable {
    public final UserRef ref;
    public final String email;

    public User(UserRef ref, String email) {
        this.ref = ref;
        this.email = email;
    }
}
