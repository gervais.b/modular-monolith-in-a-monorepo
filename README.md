# Java and Scala modular monolith in a monorepo

> A project to define a modular monolith with Java and Scala. The system uses 
> **Event sourcing** and **CQRS** with Akka and Akka-persistence in some modules.
> But, also some simpler persistence mechanism based on plain SQL.


This project borrows terms from the https://c4model.com/ and _Domain Driven Design_.

***

## Project structure

This project structure apply the same hierarchy as the C4 model diagrams. The 
root is called the _system_ while executable artifacts are called _applications_.

    .
    |- project
    |  |- application-plugin   An SBT Auto plugin to configure "applications" and
    |  |                       generate classes.
    |- {application}
    |  |- {component}-api      Describe a component API.
    |  |  |- src
    |  |  |  |- main
    |  |  |  |  |- java        Optional java sources describing the services and  
    |  |  |  |  |              objects provided by this component.
    |  |  |  |  |- oas         Optionaly contains the OpenAPI specification for 
    |  |  |  |  |              web api exposed by this component.
    |  |- {component}-impl     Implementation of a component.     
    |  |- webapi               Aggregates the components behind one executable.
               

### `project/application-plugin`

A plugin to add features and conventions over configuration. This plugin is a 
classical sbt project that should be edited as a standalone project (another 
project in your Ide). It is picked up by sbt via the [./project/plugins.sbt](./project/plugins.sbt) 
file.

### `{application}`

_Applications_ are into the second level of diagrams into the C4 model. They are 
separate runnable/deployable unit that executes code. They are made of _components_

### `{application}/{component}-api`

Description of a component. Any component that depends on another should depend 
on the API. The binding with the implementation is done by the executable module.

Some components that are designed to share classes are only made of one API.

#### `{application}/{component}-impl`

Implementation of a component. A module is a _Bounded Context_ is the sens of 
_DDD_. It can use any architectural style that fit his domain. His behavior is 
defined by his sibling "api" module.

A component implementation usually provide a component class that should be 
instantiated with their dependencies. This model is more clear about the 
requirements of each component.

    class AuthenticationComponent(
        secret: String, 
        databse: Connection
    ) {

      def getAuthenticationService():AuthenticationService

    }

A component that do not have any business, but only share classes can have only 
the API.

#### `{application}/webapi`

This is an executable project, it aggregates all components into the 
`WebApiComponents` classes to provide the required functionalities.

***

## Libraries management
To avoid the well known dependency hell and coherent libraries evolutions this 
project rely on a shared libraries descriptor.

One single file declare the libraries for all applications. Family of libraries 
that are never used alone can be regrouped under one single name. For simplicity 
when importing a library, single ones are also declared inside a `Seq`

    val Playframework = Seq(
      "com.typesafe" % "config" % "1.4.2",
      "com.typesafe.play" %% "play" % "2.8.16"
    )

    val Scalatest = Seq(
      "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test
    )

Each application can them import the needed libraries at their will:

    // Inside application build.sbt
    val `awesome-component` = (project in file("awesome-component"))
      .settings(
        libraryDependencies ++= Playframework ++ Scalatest 
      )


***

## TODO

- [ ] application-plugin, Generate routers and controllers from OpenAPI
- [ ] Embed swagger-ui in webapis (play-scala-server OpenAPI generator does it)
- [ ] Create manifest for each application (https://stackoverflow.com/questions/73166850/sbt-creating-manifest-mf-without-uber-jar)
- [ ] Create build-info for each application
- [ ] Configure and document `sbt-dependency-check` 
- [ ] Configure and document `scala-steward` 
